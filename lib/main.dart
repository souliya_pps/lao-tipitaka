import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'RandomImagePage.dart'; // Custom Widget for the Random Image Page

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Buddhaword',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: const PWAPage(),
    );
  }
}

class PWAPage extends StatefulWidget {
  const PWAPage({super.key});

  @override
  State<PWAPage> createState() => _PWAPageState();
}

class _PWAPageState extends State<PWAPage> {
  bool _isRedirecting = true;
  final String _currentUrl = 'https://buddhaword.netlify.app';

  @override
  void initState() {
    super.initState();
    _checkAndOpenBrowser();
  }

  /// Check if the URL has changed or needs to be opened again
  Future<void> _checkAndOpenBrowser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? lastOpenedUrl = prefs.getString('lastOpenedUrl');

    if (lastOpenedUrl != _currentUrl) {
      // Open the browser automatically if the URL is different
      await _openInChromeBrowser();
      await prefs.setString('lastOpenedUrl', _currentUrl);
    } else {
      // Otherwise, show the loader
      _openInChromeBrowser();
    }

    if (_isRedirecting == false) {
      _openInChromeBrowser();
    }
  }

  /// Open URL in Chrome
  Future<void> _openInChromeBrowser() async {
    final Uri url = Uri.parse(_currentUrl);

    if (await canLaunchUrl(url)) {
      await launchUrl(
        url,
        mode: LaunchMode.externalApplication, // Open in real Chrome browser
        webViewConfiguration: const WebViewConfiguration(
          enableJavaScript: true,
          enableDomStorage: true,
        ),
      );
    } else {
      throw 'Could not launch $_currentUrl';
    }

    // Optionally close the app after opening the browser
    setState(() {
      _isRedirecting = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _isRedirecting
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : RandomImagePage(),
    );
  }
}
